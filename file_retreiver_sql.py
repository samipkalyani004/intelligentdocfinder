import mysql.connector 
from mysql.connector import Error
import DataFrameBuilder
import os
import tagging

class FileRetreiver:
    def __init__(self,username,password,counter):
        self.username = username
        self.password = password
        self.counter = counter
        self.d = DataFrameBuilder.databasecaller()
    def processTags(self,text):
        tags = tagging.main(text)
        return tags
    def write_file(self,name,ftype,content):
        if ftype == "pdf":
            with open("temp.pdf", 'wb') as file:
                file.write(content)
                text = self.d.pdfextract("temp.pdf")
            tags = self.processTags(text)
            os.remove("temp.pdf")
            return [text,tags]
        if ftype == "docx":
            with open("temp.docx", 'wb') as file:
                file.write(content)
                text = self.d.docxextract("temp.docx")
                tags = self.processTags(text)
            os.remove("temp.docx")
            return [text,tags]
        if ftype == "pptx":
            with open("temp.pptx", 'wb') as file:
                file.write(content)
                text = self.d.pptxextract("temp.pptx")
                tags = self.processTags(text)
            os.remove("temp.pptx")
            return [text,tags]

    def readBLOB(self):
        try:
            connection = mysql.connector.connect(host='localhost',
                                                database='myfiles',
                                                user=self.username,
                                                password=self.password)

            cursor = connection.cursor()
            sql_fetch_blob_query = """SELECT * from files"""
            print(cursor)
            cursor.execute(sql_fetch_blob_query)
            print(cursor)
            print("-------------------")
            record = cursor.fetchall()
            print(record)
            print("-------------------")

            data = []
            for row in record:
                temp = []
                name = row[0]
                print(name)
                ftype = row[1]
                print(ftpe)
                title = row[2]
                print(title)
                content = row[3]
                print(content)
                temp.append(self.counter)
                self.counter = self.counter+1
                temp.append(ftype)
                temp.append("in_SQL_database")
                temp.append(title)
                results  = self.write_file(name,ftype,content)
                temp.append(results[0])
                tags = results[1]
                temp_tags = ""
                for tag in tags:
                    if(temp_tags != ""):
                        temp_tags += ","    
                    temp_tags+=tag
                temp.append(temp_tags)
                data.append(temp)
            return [data,self.counter]

        except mysql.connector.Error as error:
            print("Failed {}".format(error))

        finally:
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("connection closed")

def main(username,password,counter):
    f = FileRetreiver(username,password,counter)
    return f