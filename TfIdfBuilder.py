import math
import re
import spacy
import pandas as pd
import DataFrameBuilder

class TFIDFBuilder:
    
    def __init__(self, fileDir,db_dataframe,counter):
        self.tf = {}
        self.df = {}
        self.idf = {}
        self.nlp=spacy.load("en_core_web_sm")                  
        self.dataFrame=DataFrameBuilder.caller(fileDir,db_dataframe,counter) 
        d = DataFrameBuilder.databasecaller()
        d.tagBuilder()
        self.files=self.dataFrame['name'].tolist()
        self.terms_dict = self.terms_builder()
        self.scalar_val,self.index = self.scalar_val_Generator()
        self.returnData()

    def ngrammer(self,item):
        ngrams=[]
        for i in range(0,len(item)-2):
            ngrams.append(item[i]+" "+item[i+1])
            ngrams.append(item[i+1]+" "+item[i+2])
        return ngrams
    
    def regex_Penalizer(self,item):
        pattern=re.compile('[\W_]+')
        temp=self.dataFrame['title'][item-1].lower()
        temp+=" "
        temp+=self.dataFrame['content'][item-1].lower() 
        return pattern.sub(' ',temp)
        
    def terms_builder(self):
        terms_dict = {}
        for file_item in self.files:
            terms_dict[file_item] = self.regex_Penalizer(file_item)
            re.sub(r'[\W_]+','', terms_dict[file_item])
            words_sc=terms_dict[file_item].split()
            ngrams=self.ngrammer(words_sc)
            dumped_terms_dict=terms_dict[file_item]
            terms_dict[file_item]=[]
            final_terms = [item for item in self.nlp(dumped_terms_dict) if item.is_stop is False]
            for term in final_terms:
                terms_dict[file_item].append(term.lemma_)
            terms_dict[file_item]+=ngrams
            terms_dict[file_item]+=[self.dataFrame['title'][file_item-1].lower().split('.')[0]]
        return terms_dict
    
    def scalar_val_Generator(self):
        file_folder_index = {}
        for filename in self.terms_dict.keys():               
            file_folder_index[filename] = self.FileIndexer(enumerate(self.terms_dict[filename]))
        total_index = {}                   
        for filename in file_folder_index.keys():
            self.tf[filename] = {}
            for word in file_folder_index[filename].keys():
                self.tf[filename][word] = len(file_folder_index[filename][word])        
                if word in self.df.keys():                                          
                    self.df[word] += 1
                else:                                                               
                    self.df[word] = 1
                if word in total_index.keys():
                    if filename in total_index[word].keys():
                        total_index[word][filename].append(file_folder_index[filename][word][:])    
                    else:
                        total_index[word][filename] = file_folder_index[filename][word]            
                else:
                    total_index[word] = {filename: file_folder_index[filename][word]}               
        return self.File_Vectorizer(file_folder_index),total_index
    
    def FileIndexer(self, terms_list):
        fileIndex = {}
        for i, word in terms_list:
            if word in fileIndex.keys():                        
                fileIndex[word].append(i)
            else:                                               
                fileIndex[word] = [i]
        return fileIndex

    def File_Vectorizer(self,temp_index):
        vector_dict = {}
        for item in self.files:
            vector_dict[item] = [len(temp_index[item][string]) for string in temp_index[item].keys()]
        values = {}
        for item in self.files:
            values[item] = pow(sum(map(lambda x: x**2, vector_dict[item])),.5)
        return values


    def returnData(self): 
        for item in self.files:
            for word in self.index.keys():
                self.tf[item][word] = self.tf[item][word]/self.scalar_val[item] if word in self.tf[item].keys() else 0
                if word in self.df.keys():
                    self.idf[word] =  math.log(len(self.files)/self.df[word]) if self.df[word]!=0 else 0
                else:
                    self.idf[word] = 0
        return self.df, self.tf, self.idf,self.dataFrame