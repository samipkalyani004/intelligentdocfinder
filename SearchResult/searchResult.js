function fileOpener(lala) {
    if (lala.substring(0, 15) == 'In SQL Database')
        alert(lala)
    else
        fetch(`http://localhost:5000/open_file?pathToFile=${lala}`).then(res => {
            res.json().then(res => {
                if (res.status != 'OK')
                    alert(res.status)
            })
        })
}
function tagClicked(e, elm) {
    e.preventDefault()
    e.stopPropagation()
    if (elm.length == undefined)
        window.location.href = `../SearchResult/searchResult.html?type=tags&query=${elm.id}`
    else
        window.location.href = `../SearchResult/searchResult.html?type=tags&query=${elm[0].id}`
}

var dicting = {
    'docx': 'docx',
    'doc': 'docx',
    'odt': 'odt',
    'word': 'docx',
    'text': 'txt',
    'txt': 'txt',
    'ppt': 'pptx',
    'pptx': 'pptx',
    'pdf': 'pdf',
    'xlsx': 'xlsx',
    'xls': 'xlsx'
}
var urlParams = new URLSearchParams(window.location.search);
userQuery = urlParams.get('query')
document.getElementById("searchInput").value = userQuery
if (urlParams.get('type') == undefined)
    fetch(`http://localhost:5000/get_results?query=${userQuery}`).then(res => {
        res.json().then(res => {
            console.log(res)
            console.log(userQuery.toLowerCase())

            searchResults = res.searchResults
            if (searchResults[0] != undefined) {
                document.getElementById('resultList').innerHTML += `
            <h3 style="margin-bottom:5px;margin-top:0px;color:rgb(40,40,40)" >Top Result/s :</h3>
            `
                Object.keys(searchResults).forEach(elm => {
                    current_elm = searchResults[elm]
                    var str = ""
                    var x = current_elm[3].split(",")
                    console.log(x)
                    console.log(current_elm[0].split('.').splice(-1).pop())
                    x.map(elm => {
                        str += `<button class="tag_elm" onclick="tagClicked(event,${elm})">
                        <p class="tag_text" id=${elm}>${elm}</p>
                    </button>`
                    })
                    document.getElementById('resultList').innerHTML += `
                    <a class="siteLink" id=${current_elm[1]}
                    href="javascript:fileOpener('${current_elm[1].split('/').join('//').split('\\').join('\\\\')}');">
                    <div id="listElement">
                        <div id="notDivSummarizer" >
                            <div id="Elms">
                                <div id="leftElm">
                                    <h3 id="elmTitle">${current_elm[0]}</h3>
                                    <h5 id="elmLink">${current_elm[1]}</h5>
                                    <p id="elmContent">${current_elm[2]}...</p>
                                </div>
                                <div style="flex:1"></div>
                                <div id="rightElm">
                                    <image class="imgs_elms" src='../Images/${dicting[current_elm[0].split('.').splice(-1).pop().split(" ")[0]]}_kadam.png' height="100" width="100" />
                                </div>
                            </div>
                            <div id="overflower">
                                <div class="tag_scroller">
                                    ${str}
                                </div>
                            </div>
                        </div>
                        <div id="divSummarizer" >
                            <p style="margin-left:20px;margin-right:20px;color:gray;" >${current_elm[4].substring(6, current_elm[4].length - 4)}</p>
                        </div>
                    </div>
                </a>
                `
                })
            }
            else
                document.getElementById('resultList').innerHTML += `
                <div>No results found... try using different words</div>
                `
        })
    })
if (urlParams.get('type') == 'tags')
    fetch(`http://localhost:5000/tag_search?tags=${userQuery}`).then(res => {
        res.json().then(res => {
            console.log(res)

            searchResults = res.searchResults
            if (searchResults[0] != undefined) {
                document.getElementById('resultList').innerHTML += `
            <h3 style="margin-bottom:5px;margin-top:0px;color:rgb(40,40,40)" >Top Result/s :</h3>
            `
                Object.keys(searchResults).forEach(elm => {
                    current_elm = searchResults[elm]
                    current_elm = searchResults[elm]
                    var str = ""
                    var x = current_elm[3].split(",")
                    console.log(x)
                    x.map(elm => {
                        str += `<button class="tag_elm" onclick="tagClicked(event,${elm})">
                        <p class="tag_text" id=${elm}>${elm}</p>
                    </button>`
                    })
                    document.getElementById('resultList').innerHTML += `
                    <a class="siteLink" id=${current_elm[1]}
                        href="javascript:fileOpener('${current_elm[1].split('/').join('//').split('\\').join('\\\\')}');">
                        <div id="listElement">
                            <div id="notDivSummarizer" >
                                <div id="Elms">
                                    <div id="leftElm">
                                        <h3 id="elmTitle">${current_elm[0]}</h3>
                                        <h5 id="elmLink">${current_elm[1]}</h5>
                                        <p id="elmContent">${current_elm[2]}...</p>
                                    </div>
                                    <div style="flex:1"></div>
                                    <div id="rightElm">
                                        <image class="imgs_elms" src='../Images/${dicting[current_elm[0].split('.').splice(-1).pop().split(" ")[0]]}_kadam.png' height="100" width="100" />
                                    </div>
                                </div>
                                <div id="overflower">
                                    <div class="tag_scroller">
                                        ${str}
                                    </div>
                                </div>
                            </div>
                            <div id="divSummarizer" >
                                <p style="margin-left:20px;margin-right:20px;color:gray;" >${current_elm[4].substring(6, current_elm[4].length - 4)}</p>
                            </div>
                        </div>
                    </a>
                `
                })
            }
            else
                document.getElementById('resultList').innerHTML += `
                <div>No results found... try using different words</div>
                `
        })
    })


// <span class="dark">Microsoft</span>

function searchAgain(e) {
    window.location.href = `../SearchResult/searchResult.html?query=${e}`
}