<h1 align="center">Welcome to MegaHunt 👋</h1>
<p>
<img src="https://camo.githubusercontent.com/82d759d665d52db153f8f2e0a1f76bc732b6f9a3/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f76657273696f6e2d302e312e302d626c75652e7376673f63616368655365636f6e64733d32353932303030"/>
</p>

## Overview
> A smart document finder, the scope of which is to move towards 'sophistication' by indexing data (file/s) that can fetch the most valuable search experience.

## Features
+ An interactive web-based application which searches for documents inside the 1)file system 2)SQL db 3) Mongo db
+ Given a query, a list of ranked results (semantic search) sorted by relevance is shown
+ Files can be searched based on the tags they have (Tag-based search)
+ Files are automatically tagged based on the high-weightage words present inside the file
+ A screen is provided for updating/editing the tags for each file.
+ Documents are automatically summarized (abstractive summarization) which gives a gist of the content present in the file without opening them


`` A screen is provided wherein a user can add a new file to the database by uploading the file.
``

## Requirements

The following requirements are specified in requirements.txt and must be satisfied for this project to run.
+ [python_pptx]==0.6.18
+ [python_docx]==0.8.10
+ [pandas]==0.25.1
+ [Flask]==1.1.1
+ [pymongo]==3.10.1
+ [spacy]==2.2.3
+ [Keras]==2.2.4
+ [numpy]==1.16.5
+ [tensorflow]==2.1.0
+ [Flask_Cors]==3.0.8
+ [slate3k]==0.5.3
+ [Flask_RESTful]==0.3.7
+ [docx]==0.2.4
+ [mysql_connector_python]==8.0.19
+ [pandas]==0.25.1
+ [Pillow]==7.0.0
+ protobuf]==3.11.3



## Install
As long as the above requirements are satisfied, no need to install anything 
To install all the requirements run the following command
```sh
pip -r requirements.txt
```

## Usage

[Installation_Manual_And_Configuration_Guide](https://drive.google.com/file/d/1Zh3hA6fdFZ8XoGg2xdE4Ahlg3UxG-QDB/view?usp=sharing)<br />
[Infineon_Solution_Demo](https://drive.google.com/open?id=1WJNv73WG4Kdfpn8W24ehonmHMGiDdjaC)

**Recommended to view the above demo before usage**

Use install command (mentioned above) to run the server.

Run **python main.py** to start the server. A webpage will open in a few seconds (depending on the number of files present in your system)

**Note that the server will take time to run only for the first time, following which the server will run in 5 seconds thereafter.**

## Future Scope
+ Autocorrect ( mis-spelled words are automatically corrected )
+ Improvement of accuracy of the summarization model 

## Author

👤 **(Prof.)Kriti Srivastava, Sanjay Nayak, Samip Kalyani**


***
