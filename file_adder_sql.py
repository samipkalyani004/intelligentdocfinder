import mysql.connector 
from mysql.connector import Error

class FileAdderSql:
    def __init__(self, username, password, db_name, link):
        self.link = link
        self.username = username
        self.password = password
        self.db_name = db_name.lower()

    def insertBLOB(self):
        try:
            db_flag = False
            db_checker = mysql.connector.connect(host='localhost',
                                                user=self.username,
                                                password = self.password)
            db_chk_cur = db_checker.cursor()
            db_chk_cur.execute("SHOW DATABASES")

            for x in db_chk_cur:
                if(x[0] == self.db_name):
                    db_flag = True

            db_checker.commit()
            if(db_checker.is_connected()):
                db_chk_cur.close()
                db_checker.close()

        except mysql.connector.Error as error:
            return {"result":"Failed {}".format(error)}
        
        print("Database flag: ",db_flag)

        if(db_flag==False):
            try:
                db_create = mysql.connector.connect(host='localhost',
                                                    user=self.username,
                                                    password = self.password)
                db_create_cur = db_create.cursor()
                database_name = self.db_name
                create_query = "CREATE DATABASE "
                create_query += database_name
                db_create_cur.execute(create_query)
                db_create_cur.execute("""CREATE TABLE %s.files ( 
                                        id INT NOT NULL AUTO_INCREMENT,
                                        ftype VARCHAR(45) NOT NULL,
                                        title VARCHAR(45) NOT NULL,
                                        content LONGBLOB NOT NULL,
                                        PRIMARY KEY (id))""" % database_name)

                db_create.commit()
                if(db_create.is_connected()):                         
                    db_create_cur.close()
                    db_create.close()

            except mysql.connector.Error as error:
                print("except")
                return {"result":"Failed {}".format(error)}
                
        try:
            connection = mysql.connector.connect(host='localhost',
                                                database=self.db_name,
                                                user=self.username,
                                                password=self.password)

            cur = connection.cursor()
            sql_insert_blob_query = """ INSERT INTO files
                            (ftype, title, content) VALUES (%s,%s,%s)"""
            with open(self.link,'rb') as file:
                bin_content = file.read()

            print("Recived file link: ",self.link)
            
            print(connection.is_connected())

            content =  bin_content #utf-8 logic
            ftype = self.link.split('.')[-1]
            file_name = self.link.split('\\')[-1]

            print(file_name)

            insert_blob_tuple = (ftype, file_name, content)
            result = cur.execute(sql_insert_blob_query, insert_blob_tuple)
            connection.commit()
            if (connection.is_connected()):
                cur.close()
                connection.close()
                print("MySQL connection is closed")

            return {"result":"File inserted successfully."}

        except mysql.connector.Error as error:
            print("Failed {}".format(error))
            return{"result":"Failed {}".format(error)}
    

def main(username, password, db_name, link):
    f = FileAdderSql(username, password, db_name, link)
    return f

