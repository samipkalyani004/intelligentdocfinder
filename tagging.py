import re
import pickle
from collections import Counter
import spacy
nlp = spacy.load('en_core_web_sm')
stop = spacy.lang.en.stop_words.STOP_WORDS
NOUNS = ['NN', 'NNS', 'NNP', 'NNPS']

class Tagging:
    def __init__(self,text):
        self.nlp = nlp

    def clean_document(self,document):
        
        document = re.sub('[^A-Za-z .-]+', ' ', document)
        document = ' '.join(document.lower().split())
        document = ' '.join([i for i in document.split() if i not in stop])
        return document

    def extract_tags(self,document):

        doc=self.nlp(document)
        self.nlp.vocab["that"].is_stop=True
        self.nlp.vocab["."].is_stop=True
        self.nlp.vocab["-"].is_stop=True    
        stopped = [w.orth_ for w in doc if w.is_stop is False and w.tag_ in NOUNS and len(w) >= 4]
        word_freq = Counter(stopped)
        common_words = word_freq.most_common(10)
        temp = []
        for comm in common_words:
            temp.append(comm[0])
        return temp

def main(text):
    d = Tagging(text)
    document = d.clean_document(text)
    tags = d.extract_tags(document)
    return tags