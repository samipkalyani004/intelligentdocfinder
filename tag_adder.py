import pandas as pd
""" 
    This code takes title and the entered tags from user
    as input and adds it to the csv.
"""
class TagAdder:
    def __init__(self,title,tags):
        self.title = title
        self.tags = tags
        self.pd_tag=pd.read_csv('./Tags.csv',sep=",")
    
    """ 
        The Add function updates the  automated tags with the user entered tags.
    """

    def Add(self):
        try:
            list_pd_tag=list(self.pd_tag['title'])
            if self.title in list_pd_tag:
                id=list_pd_tag.index(self.title)
                self.pd_tag=self.pd_tag.drop([id])
                #make a new title-tag dataframe
                title_df = pd.DataFrame({'title':[self.title],'tags':[self.tags]})
                #Append it to the original dataframe and save it as a csv
                self.pd_tag = self.pd_tag.append(title_df,ignore_index = True )
                self.pd_tag.to_csv(path_or_buf='./Tags.csv',index=False)
                return {"status":"Success"}
            else:
                return {"status": "No such file."}
        except:
            return {"status": "Error"}
        
def main(title,tags):
    a = TagAdder(title,tags)
    return a