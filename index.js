let arr = 2


document.getElementById('search').onclick = function (e) {
    e.preventDefault()
    var sqlDbName = document.getElementById('sqlDbName').value
    var sqlUser = document.getElementById('sqlUser').value
    var sqlPass = document.getElementById('sqlPass').value
    var mongoDbName = document.getElementById('mongoDbName').value
    var node = document.createElement("div");
    node.style.cssText = 'text-align:center;color:#5F6368;margin-top:20px;'
    var textnode = document.createTextNode("Starting Server... Please Wait");         // Create a text node
    node.appendChild(textnode);
    document.getElementById('mainBody').append(node)
    sqlDbName = ""
    mongoDbName = ""
    let fileUrl=""
    var x=document.querySelectorAll('#fileDir')
    x.forEach(elm=>fileUrl+=elm.value+";;;")
    console.log(fileUrl)

    fetch(`http://localhost:5000/execute?fileDir=${fileUrl}&username=${sqlUser}&password=${sqlPass}&db_sql=${sqlDbName}&db_mongo=${mongoDbName}`).then(res => {
        res.json().then(res => {
            alert(res.message)
            if (res.status == "OK")
                window.location.href = './HomeScreen/index.html'
        })
    })
}

function insertBefore(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode);
}

function directory_select(e) {
    console.log(e)
    fetch(`http://localhost:5000/directory_select`)
        .then(res => {
            res.json().then(res => {
                console.log(res)
                document.querySelector('input[name="' + e + '"]').value = res.dirname
            })
        })
}

function nextAdder(e) {
    ref = document.getElementById(e)
    elm = document.createElement('a')
    //        <a class="form__group" id="fd1" style="width:70%;display:inline-block;" href="javascript:directory_select('fd1')" >
    elm.setAttribute("class", "form_group");
    elm.setAttribute("id", "fd" + arr.toString());
    elm.setAttribute("style", "width:70%;display:inline-block;margin-top:7px;");
    elm.setAttribute("href", "javascript:directory_select('" + "fd" + arr.toString() + "')");
    elm.innerHTML = `
    <input type="text" id="fileDir" name="fd${arr.toString()}" class="form__field" placeholder="Select a directory" style="cursor:pointer;color: transparent;text-shadow: 0 0 0 #212121;" >
    <label for="fileDir" class="form__label" style="cursor:pointer">Select a Directory</label>
    `
    arr = arr + 1
    insertBefore(elm, ref)
}