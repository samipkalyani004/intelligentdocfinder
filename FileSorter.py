import re
import math
import pandas
import spacy
import TfIdfBuilder

class FileSearch:
    def __init__(self,fileDir,db_dataframe,counter):
        self.strng=[]
        self.nlp=spacy.load('en_core_web_sm')
        self.TfIdfBuild = TfIdfBuilder.TFIDFBuilder(fileDir,db_dataframe,counter)
        self.idf = self.TfIdfBuild.index
        
    def stemmSorter(self, stemmed, strng):
        result = []
        for word in stemmed:
            regEx = re.compile('[\W_]+')
            temp = regEx.sub(' ',word)
            result += self.sort_Files([filename for filename in self.idf[temp].keys()], temp,0) if temp in self.idf.keys() else []
        return self.sort_Files(list(set(result)), strng, 1)
    
    def vectorBuilder(self, documents):
        word2vec = {}
        for doc in documents:
            doc2vec = [0]*len(self.TfIdfBuild.index.keys())
            for ind, term in enumerate(self.TfIdfBuild.index.keys()):
                doc2vec[ind] = self.TfIdfBuild.tf[doc][term] * self.TfIdfBuild.idf[term]
            word2vec[doc] = doc2vec
        return word2vec

    def inputVectorizer(self, query):
        pattern = re.compile('[\W_]+')
        query = pattern.sub(' ',query)
        Vec = [0]*len(query.split())
        index = 0
        for word in query.split():
            Vec[index] = self.countCalculator(word, query)
            index += 1
        iqf = [self.TfIdfBuild.idf[word] for word in self.TfIdfBuild.index.keys()]
        magnitude = pow(sum(map(lambda x: x**2, Vec)),.5)
        termfreq = [x/magnitude for x in self.tempRetreiver(self.TfIdfBuild.index.keys(), query)]                           
        return [termfreq[i]*iqf[i] for i in range(len(self.TfIdfBuild.index.keys()))]
    
    def countCalculator(self, term, query):
        count = 0
        for word in query.split():
            if word == term:
                count += 1
        return count
    
    def tempRetreiver(self, terms, query):
        temp = [0]*len(terms)
        for i,term in enumerate(terms):
            temp[i] = self.countCalculator(term, query)
        return temp    

    def sort_Files(self, resultDocs, query, fin):
        vectors = self.vectorBuilder(resultDocs)
        Vec = self. inputVectorizer(query)
        results = [[ sum([x*y for x,y in zip(vectors[item], Vec)]) if len(vectors[item])==len(Vec) else 0, item ] for item in resultDocs]
        results.sort(key=lambda x: x[0],reverse=True)
        results = [x[1] for x in results]
        return results
    
    def gramsProcessor(self,strng):
        grams=[]
        words=strng.split()
        if(len(words) is 1):                
            return grams
        if(len(words) is 2):                
            return [words[0]+" "+words[1]]
        return grams
 
    def inputHandler(self,strng):
        self.strng=strng
        strng=strng.lower()
        pattern = re.compile('[\W_]+')
        string = pattern.sub(' ',strng)
        n_grams=self.gramsProcessor(strng)                         
        stemmed=[]
        doc=self.nlp(string)                  
        stopped = [w for w in doc if w.is_stop is False]        
        for token in stopped:
            stemmed.append(token.lemma_)        
        stemmed+=n_grams
        return self.stemmSorter(stemmed,strng)
    
