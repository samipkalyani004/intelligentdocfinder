from pymongo import MongoClient
from gridfs import GridFS

class FileAdderMongo:
    def __init__(self, db_name, link):
        client = MongoClient('localhost', 27017)
        self.db = client[db_name]
        self.fs = GridFS(self.db)
        self.link = link
        self.db_name = db_name
    def insert(self):
        list_files = self.link.split('.')
        ftype = list_files[-1]
        str_file_path = list_files[0]
        file_name = str_file_path.split("\\")[-1]
        content_type = "document/"+ ftype
        try:
            with open(self.link,'rb') as file:
                self.fs.put(file,content_type=content_type,filename=file_name)
            return {"result":"File inserted in %s MONGO database." % self.db_name }
        except exception:
            return {"result":"Failed: {}".format(exception)}
def main(db_name,link):
    f = FileAdderMongo(db_name,link)
    return f 