document.getElementById('searchBoxInput').onfocus = function () {
    console.log('clicked')
    document.getElementById('fullSearchBox').style.boxShadow = "0 1px 6px 0 rgba(32,33,36,0.28)"
    document.getElementById('fullSearchBox').style.borderColor = "rgba(223,225,229,0)"
    document.getElementById('dropdown').style.display = "block"
}
// document.getElementById('searchBoxInput').onblur =function(){
//     console.log('unclicked')
//     document.getElementById('fullSearchBox').style.boxShadow="none"
//     document.getElementById('fullSearchBox').style.borderColor="#dfe1e5"
//     document.getElementById('dropdown').style.display="none"
// }
document.getElementById('UserScreen').onsubmit = function (e) {
    e.preventDefault()
    window.location.href = `../UserScreens/index.html`

}
var urlParams = new URLSearchParams(window.location.search);
if (urlParams.has('query')) {
    window.location.href = `../SearchResult/searchResult.html?${urlParams.toString()}`
}
function tagClicked(e) {
    e.preventDefault()
    var elm = e.target.id
    var val = document.getElementById('searchBoxInput').value
    if (val.includes(elm))
        return
    if (document.getElementById('searchBoxInput').value.trim() != "")
        document.getElementById('searchBoxInput').value += ','
    document.getElementById('searchBoxInput').value += elm
}

document.getElementById('directSearch').onclick = function () {
    window.location.href = `../SearchResult/searchResult.html?type=tags&query=${document.getElementById('searchBoxInput').value}`
}

document.getElementById('searchBtn').onclick = function (e) {
    window.location.href += `?query=${document.getElementById('searchBoxInput').value}`
}

fetch(`http://localhost:5000/get_tags`).then(res => {
    res.json().then(res => {
        console.log(res)
        var tags = res[1]
        var arr_tags = []
        var temp = []
        tags.map(elm => {
            temp = elm.split(",").splice(0, 4)
            temp.map(e => {
                if (arr_tags.indexOf(e+',') == -1||arr_tags.indexOf(','+e)==-1) {
                    arr_tags += "," + e
                }
            })
        })
        var lister = arr_tags.split(",").sort()
        var str = ""
        lister.slice(1).map(elm => {
            str += `
                <button class="tag_elm" onclick="tagClicked(event)">
                    <p class="tag_text" id=${elm}>${elm}</p>
                </button>
                `
        })
        document.getElementById('oldSearches').innerHTML = str

    })
})