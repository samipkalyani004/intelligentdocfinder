import numpy as np
import pandas as pd
import pickle
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from tensorflow.keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
encoder_model = load_model('encoder.h5',compile=False)
decoder_model = load_model('decoder.h5',compile=False)

def decode_sequence(text):
    with open('file3.pkl', 'rb') as handle:
        x_tokenizer = pickle.load(handle)
    with open('file1.pkl', 'rb') as handle:
        target_word_index = pickle.load(handle)    
    with open('file2.pkl', 'rb') as handle:
        reverse_target_word_index = pickle.load(handle)
    max_text_len=100
    post_data2=pd.DataFrame({'text':[text]})
    x_2 = np.array(post_data2['text'])
    x_3 = x_tokenizer.texts_to_sequences(x_2)
    x_3 = pad_sequences(x_3,maxlen=max_text_len,padding='post')
    input_seq = x_3.reshape(1,max_text_len)

    max_summary_len=15

    e_out, e_h, e_c = encoder_model.predict(input_seq)
    
    target_seq = np.zeros((1,1))

    target_seq[0, 0] = target_word_index['sostok']


    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
      
        output_tokens, h, c = decoder_model.predict([target_seq] + [e_out, e_h, e_c])
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_token = reverse_target_word_index[sampled_token_index]
        
        if(sampled_token!='eostok'):
            decoded_sentence += ' '+sampled_token

        if (sampled_token == 'eostok'  or len(decoded_sentence.split()) >= (max_summary_len-1)):
            stop_condition = True

        target_seq = np.zeros((1,1))
        target_seq[0, 0] = sampled_token_index

        e_h, e_c = h, c

    return decoded_sentence
