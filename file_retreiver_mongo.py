import pymongo 
import DataFrameBuilder
from gridfs import GridFS
import os
import tagging

class FileRetreiverMongo:
    def __init__(self,counter):
        self.client = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = self.client['myfiles']
        self.fs = GridFS(self.db)
        self.d = DataFrameBuilder.databasecaller()
        self.counter = counter
    def processTags(self,text):
        tags = tagging.main(text)
        return tags
    def readMongo(self):
        data_frame = []
        for grid_out in self.fs.find({"contentType" : "document/pdf"},no_cursor_timeout=True):
            temp = []
            temp.append(self.counter)
            self.counter = self.counter + 1
            ftype = "pdf"
            temp.append(ftype)
            temp.append("in_MONGO_database")
            name = grid_out.filename
            temp.append(name)
            data = grid_out.read()
            f = open('temp2.pdf', 'wb')
            f.write(data)
            f.close()
            text = self.d.pdfextract("temp2.pdf")
            temp.append(text)
            tags = self.processTags(text)
            temp_tags = ""
            for tag in tags:
                if(temp_tags != ""):
                    temp_tags += ","    
                temp_tags+=tag
            temp.append(temp_tags)
            os.remove("temp2.pdf")
            data_frame.append(temp)
        for grid_out in self.fs.find({"contentType" : "document/text"},no_cursor_timeout=True):
            temp = []
            temp.append(self.counter)
            self.counter = self.counter + 1
            ftype = "txt"
            temp.append(ftype)
            temp.append("in_MONGO_database")
            name = grid_out.filename
            temp.append(name)
            data = grid_out.read()
            f = open('temp2.txt', 'wb')
            f.write(data)
            f.close()
            f = open('temp2.txt', 'r')
            text = f.read()
            temp.append(text)
            tags = self.processTags(text)
            temp_tags = ""
            for tag in tags:
                if(temp_tags != ""):
                    temp_tags += ","    
                temp_tags+=tag
            temp.append(temp_tags)
            f.close()
            os.remove("temp2.txt")
            data_frame.append(temp)
        for grid_out in self.fs.find({"contentType" : "document/docx"},no_cursor_timeout=True):
            temp =[]
            temp.append(self.counter)
            self.counter = self.counter + 1
            ftype = "docx"
            temp.append(ftype)
            temp.append("in_MONGO_database")
            name = grid_out.filename
            temp.append(name)
            data = grid_out.read()
            f = open('temp2.docx', 'wb')
            f.write(data)
            f.close()
            text = self.d.docxextract("temp2.docx")
            temp.append(text)
            tags = self.processTags(text)
            temp_tags = ""
            for tag in tags:
                if(temp_tags != ""):
                    temp_tags += ","    
                temp_tags+=tag
            temp.append(temp_tags)
            os.remove("temp2.docx")
            data_frame.append(temp)
        for grid_out in self.fs.find({"contentType" : "document/pptx"},no_cursor_timeout=True):
            temp = []
            temp.append(self.counter)
            self.counter = self.counter + 1
            ftype = "pptx"
            temp.append(ftype)
            temp.append("in_MONGO_database")
            name = grid_out.filename
            temp.append(name)
            data = grid_out.read()
            f = open('temp2.pptx', 'wb')
            f.write(data)
            f.close()
            text = self.d.pptxextract("temp2.pptx")
            temp.append(text)
            tags = self.processTags(text)
            temp_tags = ""
            for tag in tags:
                if(temp_tags != ""):
                    temp_tags += ","    
                temp_tags+=tag
            temp.append(temp_tags)
            os.remove("temp2.pptx")
            data_frame.append(temp)
        return [data_frame,self.counter]
def main(counter):
    f = FileRetreiverMongo(counter)
    return f 
